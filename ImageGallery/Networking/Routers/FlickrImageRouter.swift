//
//  FlickrImageRouter.swift
//  ImageGallery
//
//  Created by Lucas Correa on 14/01/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import Foundation

enum FlickrImageRouter: Router {
    case searchPhotos(tags: String, page: String)
    case getSizes(photoId: String)
}

extension FlickrImageRouter {
    
    //
    // MARK: - Properties
    var method: String {
        switch self {
            case .searchPhotos, .getSizes:
                return "GET"
        }
    }
    
    var host: String {
        switch self {
            case .searchPhotos, .getSizes:
                return "api.flickr.com"
        }
    }
    
    var path: String {
        switch self {
            case .searchPhotos, .getSizes:
                return "/services/rest/"
        }
    }
    
    var scheme: String {
        switch self {
            case .searchPhotos, .getSizes:
                return "https"
        }
    }
    
    var headers: [String: String] {
        return ["Accept": "application/json",
                "Content-Type": "application/json"]
    }
    
    var parameters: [String: String] {
        switch self {
            case .searchPhotos(let tags, let page):
                return [
                    "method": Config.SEARCH_METHOD,
                    "api_key": Config.API_KEY,
                    "tags": tags,
                    "page": page,
                    "per_page": "30",
                    "format": "json",
                    "nojsoncallback": "1"
            ]
            case .getSizes(let photoId):
                return [
                    "method": Config.GET_SIZES_METHOD,
                    "api_key": Config.API_KEY,
                    "photo_id": photoId,
                    "format": "json",
                    "nojsoncallback": "1"
            ]
        }
    }
    
    var urlComponents: URLComponents {
        var urlComponents = URLComponents()
        
        switch self {
            case .searchPhotos, .getSizes:
                urlComponents.scheme = scheme
                urlComponents.host = host
                urlComponents.path = path
                if parameters.count > 0 {
                    urlComponents.setQueryItems(with: parameters)
                }
                return urlComponents
        }
    }
}
