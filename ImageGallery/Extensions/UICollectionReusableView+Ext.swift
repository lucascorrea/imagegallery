//
//  UICollectionReusableView+Ext.swift
//  ImageGallery
//
//  Created by Lucas Correa on 29/08/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import UIKit

extension UICollectionReusableView {
    
    static var reuseViewIdentifier: String {
        return String(describing: self)
    }
    
}
