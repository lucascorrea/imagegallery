//
//  SizeContainerTests.swift
//  ImageGalleryTests
//
//  Created by Lucas Correa on 16/01/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import XCTest

@testable import ImageGallery

class SizeContainerTests: ModelCustomTests {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testSizeContainerHasExpectedToBeEmpty() {
        let sizeContainer = SizeContainer(sizeList: [])
        XCTAssertTrue(sizeContainer.sizeList.count == 0)
    }
    
    func testSizeContainerHasExpectedTheSizeList() {
        let sizeContainer = makeSUT()
        XCTAssertTrue(sizeContainer.sizeList.count == 1)
        
        let size = sizeContainer.sizeList.first
        XCTAssertEqual(size?.label, "Large Square")
        XCTAssertEqual(size?.width, 150)
        XCTAssertEqual(size?.height, 150)
        XCTAssertEqual(size?.source, "https://live.staticflickr.com/65535/49392635048_67b42dd973_q.jpg")
        XCTAssertEqual(size?.url, "https://www.flickr.com/photos/stolensoul/49392635048/sizes/q/")
    }
    
    func testSizeContainerDecoding() throws {
        XCTAssertNoThrow(try JSONDecoder().decode(SizeContainer.self, from: makeData(resource: "sizes", withExtension: "json")))
    }
    
    func testSizeContainerDecodingVariables() throws {
        let sizeContainer = try JSONDecoder().decode(SizeContainer.self, from: makeData(resource: "sizes", withExtension: "json"))
        
        XCTAssertTrue(sizeContainer.sizeList.count == 11)
    }

    func makeSUT() -> SizeContainer {
        
        var sizeList = [Size]()
        
        let size = Size(label: "Large Square", width: 150, height: 150, source: "https://live.staticflickr.com/65535/49392635048_67b42dd973_q.jpg", url: "https://www.flickr.com/photos/stolensoul/49392635048/sizes/q/")
        sizeList.append(size)
        
        let sizeContainer = SizeContainer(sizeList: sizeList)
        return sizeContainer
    }

}
