//
//  PhotoCollectionViewModel.swift
//  ImageGallery
//
//  Created by Lucas Correa on 14/01/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import Foundation

typealias SuccessPhotoHandler = ([IndexPath]) -> Void

class PhotoCollectionViewModel {
    
    //
    // MARK: - Properties
    var photoItems: [Photo]
    let queue: OperationQueue
    var operations: [IndexPath: [Operation]]
    var currentPage: Int
    var isFetching: Bool
    var title: String {
        return "Flickr Gallery"
    }
    
    private let service: FlickrService
    
    //
    // MARK: - Initializer DI
    init(service: FlickrService = FlickrService(client: FlickrClient())) {
        self.service = service
        self.photoItems = [Photo]()
        self.currentPage = 1
        self.queue = OperationQueue()
        self.operations = [:]
        self.isFetching = false
    }
    
    //
    // MARK: - Public Functions
    
    func operationsCancel(indexPath: IndexPath) {
        if let operations = operations[indexPath] {
            operations.forEach { $0.cancel() }
        }
    }
    
    /// Clear all photos
    func removeAllPhotos() {
        photoItems.removeAll()
    }
    
    /// Photo list
    /// - Parameters:
    ///   - tags: Term from search
    ///   - page: pagination
    ///   - success: Closure Void
    ///   - failure: Closure Errors
    func searchPhotos(by tags: String, page: String, success: @escaping SuccessPhotoHandler, failure: @escaping FailureHandler) {
        service.searchPhotos(by: tags, page: page, completion: { result in
            switch result {
                case .success(let photoContainer):
                    
                    if photoContainer.photoList.count == 0 {
                        success([])
                        return
                    }
                    
                    let photoStartIndex = self.photoItems.count
                    let photoEndIndex = photoStartIndex + photoContainer.photoList.count - 1
                    let newIndexPaths = (photoStartIndex...photoEndIndex)
                        .map { return IndexPath(row: $0, section: 0) }
                    
                    self.photoItems += photoContainer.photoList
                    
                    success(newIndexPaths)
                case .failure(let error):
                    failure(error)
            }
        })
    }
}
