//
//  FlickrService.swift
//  ImageGallery
//
//  Created by Lucas Correa on 14/01/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import Foundation

typealias ResultPhotoList = Result<(PhotoContainer), NetworkError>
typealias ResultSizeList = Result<(SizeContainer), NetworkError>

class FlickrService: Service {
    
    //
    // MARK: - Properties
    var client: NetworkClient
    
    //
    // MARK: - Initializer DI
    required init(client: NetworkClient) {
        self.client = client
    }
    
    //
    // MARK: - Public Functions
    
    func cancelProcess() {
        client.cancelNetworkRequest()
    }
    
    /// Photo list - Search Photo by tags
    /// - Parameters:
    ///   - tags: Term from search
    ///   - page: pagination
    ///   - completion: Closure
    func searchPhotos(by tags: String, page: String, completion: @escaping (ResultPhotoList) -> Void) {
        client.request(router: FlickrImageRouter.searchPhotos(tags: tags, page: page), returnType: PhotoContainer.self) { result in
            completion(result)
        }
    }
    
    /// Get photo size
    /// - Parameters:
    ///   - photoId: Int
    ///   - completion: Closure
    func getPhoto(photoId: String, completion: @escaping (ResultSizeList) -> Void)  {
        client.request(router: FlickrImageRouter.getSizes(photoId: photoId), returnType: SizeContainer.self) { result in
            completion(result)
        }
    }

}
