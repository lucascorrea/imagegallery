//
//  PhotoRoot.swift
//  ImageGallery
//
//  Created by Lucas Correa on 14/01/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import Foundation

struct PhotoRoot: Codable {
    let photos: Photos
    let stat: String
}
