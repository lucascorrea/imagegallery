//
//  ImageCacheManager.swift
//  ImageGallery
//
//  Created by Lucas Correa on 30/08/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import UIKit

class ImageCacheManager: NSCache<AnyObject, UIImage> {
    
    //
    // MARK: - Singleton
    static let shared = ImageCacheManager()
    
    /// Init
    private override init() {
        super.init()
    }
    
    subscript(key: String) -> UIImage? {
        get {
            return object(forKey: key as AnyObject)
        }
        
        set (newValue) {
            if let object = newValue {
                setObject(object, forKey: key as AnyObject)
            } else {
                removeObject(forKey: key as AnyObject)
            }
        }
    }
}
