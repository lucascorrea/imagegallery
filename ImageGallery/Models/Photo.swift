//
//  Photo.swift
//  ImageGallery
//
//  Created by Lucas Correa on 14/01/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import Foundation

struct Photo {
    let id: String
    let title: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
    }
}

// MARK: - Decodable
extension Photo: Decodable {
    
}
