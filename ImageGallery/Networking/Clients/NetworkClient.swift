//
//  NetworkClient.swift
//  ImageGallery
//
//  Created by Lucas Correa on 14/01/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import Foundation

protocol NetworkClient {
    func request<T: Decodable>(router: Router, returnType: T.Type, completion: @escaping (Result<T, NetworkError>) -> Void)
    func cancelNetworkRequest()
}
