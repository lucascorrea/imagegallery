//
//  ImageDownloadOperation.swift
//  ImageGallery
//
//  Created by Lucas Correa on 29/08/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import UIKit

final class ImageDownloadOperation: AsyncOperation {
    
    //
    // MARK: - Properties
    var image: UIImage?
    private var sessionTask: URLSessionTask?
    
    //
    // MARK: - Private Methods
    
    /// Download from a image
    private func download() {
        let dependencyImageUrl = dependencies
            .compactMap { ($0 as? PhotoSizeDataProvider)?.imageUrl }
            .first
        
        guard let imageUrl = dependencyImageUrl else {
            return state = .finished
        }
        
        if let cachedImage = ImageCacheManager.shared[imageUrl.absoluteString] {
            image = cachedImage
            state = .finished
            print("Return cached Image for \(imageUrl)")
        } else {
            print("Started download for \(imageUrl)")
            sessionTask = URLSession.shared.dataTask(with: imageUrl) { data, _, error in
                guard let data = data, error == nil else { return }
                guard !self.isCancelled else { return }
                
                defer { self.state = .finished }
                
                let cachedImage = UIImage(data: data)
                self.image = cachedImage
                ImageCacheManager.shared[imageUrl.absoluteString] = cachedImage
                print("Downloaded Image for \(imageUrl)")
            }
            
            sessionTask?.resume()
        }
        
    }
    
    override func main() {
        guard isCancelled == false else { return }
        download()
    }
    
    override func cancel() {
        super.cancel()
        sessionTask?.cancel()
    }
}
