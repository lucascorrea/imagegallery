//
//  MockFlickrClient.swift
//  ImageGalleryTests
//
//  Created by Lucas Correa on 16/01/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import Foundation

@testable import ImageGallery

class MockFlickrClient: NetworkClient {
    
    var shouldReturnError = false
    
    //
    // MARK: - Public Functions
    
    /// This method is responsible for requesting with the  API.
    /// - Parameters:
    ///   - router: Router contains which method, path, headers, parameters and url will be used in the request.
    ///   - returnType: Which is the type of model returned in the result.
    ///   - completion: Result<Type of model, NetworkError>
    func request<T>(router: Router, returnType: T.Type, completion: @escaping (Result<T, NetworkError>) -> Void) where T : Decodable {
        
        let flickImageRouter = router as! FlickrImageRouter
        let bundle = Bundle(for: type(of: self))
        
        switch flickImageRouter {
        case .searchPhotos(let tag, _):
            
            if tag != "kitten" && tag != "500" {
                return completion(.failure(.decodingError(message: "The data couldn’t be read because it isn’t in the correct format.")))
            } else if tag == "500" {
                return completion(.failure(.statusCodeError(message: "StatusCode 500", code: 500)))
            }
            
            if shouldReturnError {
                return completion(.failure(.statusCodeError(message: "StatusCode 404", code: 404)))
            }
            
            guard let url = bundle.url(forResource: "search", withExtension: "json") else {
                return
            }
            
            guard let data = try? Data(contentsOf: url) else {
                return
            }
            
            guard let object = try? JSONDecoder().decode(returnType, from: data) else {
                return completion(.failure(.decodingError(message: "The data couldn’t be read because it isn’t in the correct format.")))
            }
            
            completion(.success(object))
            
        case .getSizes(let photoId):
            
            if photoId != "49392635048" && photoId != "666" {
                return completion(.failure(.decodingError(message: "The data couldn’t be read because it isn’t in the correct format.")))
            } else if photoId == "666" {
                return completion(.failure(.statusCodeError(message: "StatusCode 500", code: 500)))
            }
            
            guard let url = bundle.url(forResource: "sizes", withExtension: "json") else {
                return
            }
            
            guard let data = try? Data(contentsOf: url) else {
                return
            }
            
            guard let object = try? JSONDecoder().decode(returnType, from: data) else {
                return completion(.failure(.decodingError(message: "The data couldn’t be read because it isn’t in the correct format.")))
            }
            
            completion(.success(object))
        }
    }
    
    func cancelNetworkRequest() {
        
    }
}
