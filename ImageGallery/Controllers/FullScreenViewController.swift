//
//  FullScreenViewController.swift
//  ImageGallery
//
//  Created by Lucas Correa on 29/08/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import UIKit

typealias CompletionHandler = () -> Void

class FullScreenViewController: UIViewController {
    
    //
    // MARK: - IBOutlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    //
    // MARK: - Properties
    private let viewModel: FullScreenViewModel
    private var completion: CompletionHandler?
    
    /// Init
    /// - Parameters:
    ///   - viewModel: FullScreenViewModel
    init(viewModel: FullScreenViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("You must create this view controller with a ViewModel.")
    }
    
    //
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //
    // MARK: - Public methods
    func showImage(completionHandler: @escaping CompletionHandler) {
        completion = completionHandler
        
        viewModel.downloadImage { [weak self] image in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.activityIndicatorView.isHidden = true
                self.imageView.image = image
            }
        }
    }
    
    //
    // MARK: - IBActions
    @IBAction func closeImageAction(_ sender: Any) {
        UIView.animate(withDuration: 0.25,
                       delay: 0,
                       options: .curveEaseInOut,
                       animations: {
                        self.imageView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
                        self.imageView.alpha = 0
        }) { _ in
            UIView.animate(withDuration: 0.15,
                           animations: {
                            self.view.alpha = 0
            }) { _ in
                self.viewModel.queue.cancelAllOperations()
                self.completion?()
                self.view.removeFromSuperview()
            }
        }
    }
}

// MARK: - UIScrollViewDelegate
extension FullScreenViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}
