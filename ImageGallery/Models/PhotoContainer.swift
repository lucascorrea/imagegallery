//
//  PhotoContainer.swift
//  ImageGallery
//
//  Created by Lucas Correa on 14/01/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import Foundation

//https://developer.apple.com/documentation/foundation/archives_and_serialization/encoding_and_decoding_custom_types

struct PhotoContainer {
    let page: Int
    let pages: Int
    let perpage: Int
    let total: String
    let photoList: [Photo]
    
    enum CodingKeys: String, CodingKey {
        case page
        case pages
        case perpage
        case total
        case photos = "photos"
        case photoList = "photo"
    }
}

// MARK: - Decodable
extension PhotoContainer: Decodable {
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let photos = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .photos)
        
        photoList = try photos.decode([Photo].self, forKey: .photoList)
        page = try photos.decode(Int.self, forKey: .page)
        pages = try photos.decode(Int.self, forKey: .pages)
        perpage = try photos.decode(Int.self, forKey: .perpage)
        total = try photos.decode(String.self, forKey: .total)
    }
    
}
