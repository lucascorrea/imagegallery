//
//  FullScreenViewModel.swift
//  ImageGallery
//
//  Created by Lucas Correa on 30/08/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import UIKit

class FullScreenViewModel {
    
    //
    // MARK: - Properties
    let photo: Photo
    let queue: OperationQueue
    private var downloadCompletion: DownloadHandler?
    
    //
    // MARK: - Initializer DI
    init(photo: Photo) {
        self.photo = photo
        self.queue = OperationQueue()
    }
    
    //
    // MARK: - Public methods
    
    /// Download image
    /// - Parameters:
    ///   - completion: Closure
    func downloadImage(completion: @escaping DownloadHandler) {
        downloadCompletion = completion
        let imageDownloadFacade = ImageDownloadFacade()
        imageDownloadFacade.downloadImage(photoId: photo.id, size: .large, queue: queue) { image in
            self.downloadCompletion?(image)
        }
    }
}
