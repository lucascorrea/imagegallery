//
//  SearchCollectionReusableView.swift
//  ImageGallery
//
//  Created by Lucas Correa on 15/01/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import UIKit

class SearchCollectionReusableView: UICollectionReusableView {
    
    //
    // MARK: - IBOultets
    @IBOutlet weak var searchBar: UISearchBar!
    
}
