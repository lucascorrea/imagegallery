//
//  PhotoCollectionViewCellViewModel.swift
//  ImageGallery
//
//  Created by Lucas Correa on 31/08/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import UIKit

class PhotoCollectionViewCellViewModel {
    
    //
    // MARK: - Properties
    let photo: Photo
    var operations: [Operation]
    private var successCompletion: SuccessHandler?
    
    //
    // MARK: - Initializer DI
    init(photo: Photo) {
        self.photo = photo
        self.operations = [Operation]()
    }
    
    //
    // MARK: - Public methods
    
    /// Download image with OperationQueue
    /// - Parameters:
    ///   - queue: OperationQueue
    ///   - completion: Closure
    func downloadImage(queue: OperationQueue, completion: @escaping SuccessHandler) {
        successCompletion = completion
        
        let imageDownloadFacade = ImageDownloadFacade()
        imageDownloadFacade.downloadImage(photoId: photo.id, size: .largeSquare, queue: queue) { _ in
            self.successCompletion?()
        }
        operations = imageDownloadFacade.getOperations()
    }
}
