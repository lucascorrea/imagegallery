//
//  PhotoTests.swift
//  ImageGalleryTests
//
//  Created by Lucas Correa on 16/01/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import XCTest

@testable import ImageGallery

class PhotoTests: ModelCustomTests {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testPhotoHasExpectedToBeEmpty() {
        let photo = Photo(id: "", title: "")
        XCTAssertEqual(photo.id, "")
        XCTAssertEqual(photo.title, "")
    }
    
    func testPhotoHasExpectedTheId() {
        let photo = makeSUT()
        XCTAssertEqual(photo.id, "49392635048")
        XCTAssertNotEqual(photo.id, "49392635049")
    }
    
    func testPhotoHasExpectedTheTitle() {
        let photo = makeSUT()
        XCTAssertEqual(photo.title, "Babushka cat doll")
        XCTAssertNotEqual(photo.title, "Bluth")
    }
    
    func testPhotoEncoding() throws {
        let photo = makeSUT()
        XCTAssertNoThrow(try JSONEncoder().encode(photo))
    }
    
    func testPhotoDecoding() throws {
        XCTAssertNoThrow(try JSONDecoder().decode(Photo.self, from: makeData(resource: "photo", withExtension: "json")))
    }
    
    func testPhotoDecodingVariables() throws {
        let photo = try JSONDecoder().decode(Photo.self, from: makeData(resource: "photo", withExtension: "json"))
        
        XCTAssertEqual(photo.id, "49392635048")
        XCTAssertEqual(photo.title, "Babushka cat doll")
    }
    
    func makeSUT() -> Photo {
        let photo = Photo(id: "49392635048", title: "Babushka cat doll")
        return photo
    }
}

extension Photo: Encodable {
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(title, forKey: .title)
    }
}
