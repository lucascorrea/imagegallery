//
//  Constants.swift
//  ImageGallery
//
//  Created by Lucas Correa on 14/01/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import Foundation

enum Config {
    static let API_KEY: String = "f9cc014fa76b098f9e82f1c288379ea1"
    static let TIMEOUT: TimeInterval = 60.0
    static let SEARCH_METHOD = "flickr.photos.search"
    static let GET_SIZES_METHOD = "flickr.photos.getSizes"
}



