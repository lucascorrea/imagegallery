//
//  ModelCustomTests.swift
//  ImageGalleryTests
//
//  Created by Lucas Correa on 16/01/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import XCTest

class ModelCustomTests: XCTestCase {

    func makeData(resource: String, withExtension: String) -> Data {
        let bundle = Bundle(for: type(of: self))
        guard let url = bundle.url(forResource: resource, withExtension: withExtension) else {
            return Data()
        }
        
        guard let data = try? Data(contentsOf: url) else {
            return Data()
        }

        return data
    }
}
