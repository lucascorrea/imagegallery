//
//  UICollectionView+Ext.swift
//  ImageGallery
//
//  Created by Lucas Correa on 29/08/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import UIKit

extension UICollectionView {
    
    func dequeueReusableCell<T: UICollectionViewCell>(forIndexPath indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath) as! T
    }
    
    func dequeueReusableSupplementaryView<T: UICollectionReusableView>(ofKind elementKind: String, forIndexPath indexPath: IndexPath) -> T {
        return self.dequeueReusableSupplementaryView(ofKind: elementKind, withReuseIdentifier: T.reuseViewIdentifier, for: indexPath) as! T
    }
    
    func register(_ cell: UICollectionViewCell.Type) {
        let nib = UINib(nibName: cell.reuseIdentifier, bundle: nil)
        register(nib, forCellWithReuseIdentifier: cell.reuseIdentifier)
    }
    
    func registerHeader(_ reusableView: UICollectionReusableView.Type) {
        let nib = UINib(nibName: reusableView.reuseViewIdentifier, bundle: nil)
        register(nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: reusableView.reuseViewIdentifier)
    }
}
