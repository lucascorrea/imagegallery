//
//  FlickrImageRouterTests.swift
//  ImageGalleryTests
//
//  Created by Lucas Correa on 16/01/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import XCTest

@testable import ImageGallery

class FlickrImageRouterTests: XCTestCase {
    
    let flickrSearchRouter = FlickrImageRouter.searchPhotos(tags: "kitten", page: "1")
    let flickrGetSizesRouter = FlickrImageRouter.getSizes(photoId: "49392635048")

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testFlickrSearchRouterHasExpectedPath() {
        XCTAssertEqual(flickrSearchRouter.path, "/services/rest/")
    }
    
    func testFlickrSearchRouterHasExpectedParameters() {
        XCTAssert(flickrSearchRouter.parameters.count == 7)
    }
    
    func testFlickrSearchRouterHasExpectedHeaders() {
        XCTAssert(flickrSearchRouter.headers == ["Accept": "application/json",
        "Content-Type": "application/json"])
    }
    
    func testFlickrSearchRouterHasExpectedMethod() {
        XCTAssertEqual(flickrSearchRouter.method, "GET")
    }
    
    func testFlickrGetSizesRouterHasExpectedPath() {
        XCTAssertEqual(flickrGetSizesRouter.path, "/services/rest/")
    }
    
    func testFlickrGetSizesRouterHasExpectedParameters() {
        XCTAssert(flickrGetSizesRouter.parameters.count == 5)
    }
    
    func testFlickrGetSizesRouterHasExpectedHeaders() {
        XCTAssert(flickrGetSizesRouter.headers == ["Accept": "application/json",
        "Content-Type": "application/json"])
    }
    
    func testFlickrGetSizesRouterHasExpectedMethod() {
        XCTAssertEqual(flickrGetSizesRouter.method, "GET")
    }

}
