//
//  FlickrServiceTests.swift
//  ImageGalleryTests
//
//  Created by Lucas Correa on 16/01/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import XCTest

@testable import ImageGallery

class FlickrServiceTests: XCTestCase {
    
    var flickrClient: MockFlickrClient!
    var flickrService: FlickrService!
    
    override func setUp() {
        flickrClient = MockFlickrClient()
    }
    
    override func tearDown() {
        flickrClient = nil
    }
    
    func testFlickrServiceHasExpectedRequestPhotoListCount() {
        flickrService = FlickrService(client: flickrClient)
        
        flickrService.searchPhotos(by: "kitten", page: "1") { result in
            switch result {
            case .success(let photoContainer):
                XCTAssert(photoContainer.photoList.count == 5)
            case .failure(_):
                XCTFail("ProductService has expected request clusters count equal 5")
            }
        }
    }
    
    func testFlickrServiceHasExpectedRequestTagDecodingError() {
        flickrService = FlickrService(client: flickrClient)
        
        flickrService.searchPhotos(by: "decoding error", page: "1") { result in
            switch result {
            case .success(_):
                XCTFail("")
            case .failure(let error):
                XCTAssert(error.localizedDescription == "Decoding error. The data couldn’t be read because it isn’t in the correct format.")
            }
        }
    }
    
    func testFlickrServiceHasExpectedRequestTagFailure() {
        flickrService = FlickrService(client: flickrClient)
        
        flickrService.searchPhotos(by: "500", page: "1") { result in
            switch result {
            case .success(_):
                XCTFail("")
            case .failure(let error):
                XCTAssert(error.localizedDescription == "GET/POST request not successful. 500 StatusCode 500")
            }
        }
    }
    
    func testFlickrServiceHasExpectedRequestSizesCount() {
        flickrService = FlickrService(client: flickrClient)
        
        flickrService.getPhoto(photoId: "49392635048") { result in
            switch result {
            case .success(let sizeContainer):
                XCTAssert(sizeContainer.sizeList.count == 11)
            case .failure(_):
                XCTFail("ProductService has expected request clusters count equal 5")
            }
        }
    }
    
    func testFlickrServiceHasExpectedRequestPhotoIdDecodingError() {
        flickrService = FlickrService(client: flickrClient)
        
        flickrService.getPhoto(photoId: "500000") { result in
            switch result {
            case .success(_):
                XCTFail("")
            case .failure(let error):
                XCTAssert(error.localizedDescription == "Decoding error. The data couldn’t be read because it isn’t in the correct format.")
            }
        }
    }
    
    func testFlickrServiceHasExpectedRequestPhotoIdFailure() {
        flickrService = FlickrService(client: flickrClient)
        
        flickrService.getPhoto(photoId: "666") { result in
            switch result {
            case .success(_):
                XCTFail("")
            case .failure(let error):
                XCTAssert(error.localizedDescription == "GET/POST request not successful. 500 StatusCode 500")
            }
        }
    }
    
}
