# Mindera-challenge

This is the project to the Mindera challenge described below.

[![Build Status](https://app.bitrise.io/app/c316c4aa50ea9be3/status.svg?token=S5fGwzZs-jawyc4ekzq2ww)](https://app.bitrise.io/app/c316c4aa50ea9be3)

<a href="https://swift.org"><img src="https://img.shields.io/badge/Swift-5-orange.svg?style=flat"></a>

  
## Demo

http://www.lucascorrea.com/images/ImageGallery.gif

![Mindera Demo](http://www.lucascorrea.com/images/ImageGallery.gif)


## Build:

- Xcode version  11.6
- Design pattern MVVM
- Swift 5
  

## Bitrise - CI/CD

To test Bitrise offline you first need to have Bitrise CLI installed:
Installing with Homebrew:

`brew update && brew install bitrise`

In the folder of the cloned project to test the bitrise enter the command:

`bitrise run primary`

## Mindera - iOS Code Challenge

Hi there!


For this reapply phase of our process, we would like you to (re)create a simple app. We will give you a set of requirements and also some “nice to have”s. Please note that you don’t have to do everything, since we are more concerned with quality over quantity. Be prepared to justify your decisions and thought process.

Ok, let’s get to the point.  

The app should be made of a single screen to list a set of images fetched from a JSON API, where the images should be shown in a grid layout (2 per row).

  
# API

  
The API to be used is from Flickr. You may need to use 2 endpoints to get to the images’ URLs:


1.  [flickr.photos.search](https://www.flickr.com/services/api/flickr.photos.search.html)


2.  [flickr.photos.getSizes](https://www.flickr.com/services/api/flickr.photos.getSizes.html)

API Key: f9cc014fa76b098f9e82f1c288379ea1
  

# NOTES


- On this version, you should allow searching for images using the parameter tags.

- The image tiles should use the images with label “Large Square”

- You can paginate the results using the page parameter.

  

# EXAMPLES
  

Get the first page of kitten images

[https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=f9cc014fa76b098f9e82f1c288379ea1&tags=kitten&page=1&format=json&nojsoncallback=1](https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=f9cc014fa76b098f9e82f1c288379ea1&tags=kitten&page=1&format=json&nojsoncallback=1)


Get Sizes/URLs for a single image

[https://api.flickr.com/services/rest/?method=flickr.photos.getSizes&api_key=f9cc014fa76b098f9e82f1c288379ea1&photo_id=31456463045&format=json&nojsoncallback=1](https://api.flickr.com/services/rest/?method=flickr.photos.getSizes&api_key=f9cc014fa76b098f9e82f1c288379ea1&photo_id=31456463045&format=json&nojsoncallback=1)


Get the image data with the label “Large Square”

[https://farm6.staticflickr.com/5800/31456463045_5a0af4ddc8_q.jpg](https://farm6.staticflickr.com/5800/31456463045_5a0af4ddc8_q.jpg)

  

# REQUIREMENTS

  

## Must have


- UI
    - Must be implemented in UIKit (no SwiftUI for now)
    - Grid layout (2 images per row)
    - Should be memory efficient
    - Tap an image and open it in full screen (use the image with “Large” label).
    - Infinite scrolling
- Network Layer
    - Implement the necessary logic to perform the API requests for fetching the image list and image data
    - Caching (cache response data to avoid hitting the network every time)
- Parsing
    - Implement the necessary logic to perform the API response parsing
- Unit Tests
    - Your code should ideally have unit tests or at least be easily testable
- Language
    - Main target must be written in Swift, but you can still use Objective-C dependencies
- Version Control
    - The app should be under a git repository.
    - You can host the repo online, but please mark it as private.
- Dependencies
    - Don’t use any 3rd party framework for networking, image preview or persistence

 
## Nice to have (Extras)

- Offline mode (use any existent images from a previous run, if no network available)
- Certificate pinning
- Support for landscape mode
  

### Extra features that I did

- Integration with Bitrise CI
- I added ImageGalleryApp logo
- Certificate pinning
