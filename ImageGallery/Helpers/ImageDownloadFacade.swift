//
//  ImageDownloadFacade.swift
//  ImageGallery
//
//  Created by Lucas Correa on 05/09/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import UIKit

typealias DownloadHandler = (UIImage?) -> Void

class ImageDownloadFacade {
    
    //
    // MARK: - Properties
    private var operations: [Operation] = []
    private var downloadCompletion: DownloadHandler?
    
    //
    // MARK: - Public methods
    
    func downloadImage(photoId: String, size: SizeLabel, queue: OperationQueue, completion: @escaping DownloadHandler) {
        downloadCompletion = completion
        let photoSizeOperation = PhotoSizeOperation(photoId: photoId,
                                                    sizeLabel: size)
        let imageDownloadOperation = ImageDownloadOperation()
        imageDownloadOperation.addDependency(photoSizeOperation)
        imageDownloadOperation.completionBlock = { self.downloadCompletion?(imageDownloadOperation.image)
        }
        operations = [photoSizeOperation, imageDownloadOperation]
        queue.addOperation(photoSizeOperation)
        queue.addOperation(imageDownloadOperation)
    }
    
    func getOperations() -> [Operation] {
        return operations
    }
}
