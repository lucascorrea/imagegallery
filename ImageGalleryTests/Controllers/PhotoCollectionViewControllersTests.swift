//
//  PhotoCollectionViewControllersTests.swift
//  ImageGalleryTests
//
//  Created by Lucas Correa on 16/01/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import XCTest

@testable import ImageGallery

class PhotoCollectionViewControllersTests: XCTestCase {
    
    let window = UIWindow(frame: UIScreen.main.bounds)
    var viewController: PhotoCollectionViewController!
    var flickrClient: MockFlickrClient!
    var service: FlickrService!
    
    override func setUp() {
        flickrClient = MockFlickrClient()
        service = FlickrService(client: flickrClient)
        
        viewController = PhotoCollectionViewController(viewModel: PhotoCollectionViewModel(service: self.service))
        
        window.makeKeyAndVisible()
        window.rootViewController = viewController
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testPhotoCollectionViewControllerShouldBePresented() {
        viewController.viewDidLoad()
        XCTAssertNotNil(viewController.view)
    }
    
}
