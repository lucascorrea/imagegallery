//
//  PhotoContainerTests.swift
//  ImageGalleryTests
//
//  Created by Lucas Correa on 15/01/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import XCTest
@testable import ImageGallery

class PhotoContainerTests: ModelCustomTests {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testPhotoContainerHasExpectedToBeEmpty() {
        let photoContainer = PhotoContainer(page: 0, pages: 0, perpage: 0, total: "", photoList: [])
        XCTAssertEqual(photoContainer.page, 0)
        XCTAssertEqual(photoContainer.pages, 0)
        XCTAssertEqual(photoContainer.perpage, 0)
        XCTAssertEqual(photoContainer.total, "")
        XCTAssertTrue(photoContainer.photoList.count == 0)
    }
    
    func testPhotoContainerHasExpectedThePage() {
        let photoContainer = makeSUT()
        XCTAssertEqual(photoContainer.page, 1)
        XCTAssertNotEqual(photoContainer.page, 2)
    }
    
    func testPhotoContainerHasExpectedThePages() {
        let photoContainer = makeSUT()
        XCTAssertEqual(photoContainer.pages, 10050)
        XCTAssertNotEqual(photoContainer.pages, 10051)
    }
    
    func testPhotoContainerHasExpectedTheTotal() {
        let photoContainer = makeSUT()
        XCTAssertEqual(photoContainer.total, "1004974")
        XCTAssertNotEqual(photoContainer.total, "1004975")
    }
    
    func testPhotoContainerHasExpectedThePhotoList() {
        let photoContainer = makeSUT()
        XCTAssertTrue(photoContainer.photoList.count == 1)
        
        let photo = photoContainer.photoList.first
        XCTAssertEqual(photo?.id, "49392635048")
        XCTAssertEqual(photo?.title, "Babushka cat doll")
        
        XCTAssertNotEqual(photoContainer.total, "1004975")
    }
    
    func testPhotoContainerEncoding() throws {
        let photoContainer = makeSUT()
        XCTAssertNoThrow(try JSONEncoder().encode(photoContainer))
    }
    
    func testPhotoContainerDecoding() throws {
        XCTAssertNoThrow(try JSONDecoder().decode(PhotoContainer.self, from: makeData(resource: "search", withExtension: "json")))
    }
    
    func testPhotoContainerDecodingVariables() throws {
        let photoContainer = try JSONDecoder().decode(PhotoContainer.self, from: makeData(resource: "search", withExtension: "json"))
        
        XCTAssertEqual(photoContainer.page, 1)
        XCTAssertEqual(photoContainer.pages, 10050)
        XCTAssertEqual(photoContainer.perpage, 100)
        XCTAssertEqual(photoContainer.total, "1004974")
        XCTAssertTrue(photoContainer.photoList.count == 5)
    }
    
    func makeSUT() -> PhotoContainer {
        var photoList = [Photo]()
        
        let photo = Photo(id: "49392635048", title: "Babushka cat doll")
        photoList.append(photo)
        
        let photoContainer = PhotoContainer(page: 1, pages: 10050, perpage: 100, total: "1004974", photoList: photoList)
        return photoContainer
    }
}

extension PhotoContainer: Encodable {
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        var photos = container.nestedContainer(keyedBy: CodingKeys.self, forKey: .photos)
        
        try photos.encode(photoList, forKey: .photoList)
        try photos.encode(page, forKey: .page)
        try photos.encode(pages, forKey: .pages)
        try photos.encode(perpage, forKey: .perpage)
        try photos.encode(total, forKey: .total)
    }
}
