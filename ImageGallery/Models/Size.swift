//
//  Size.swift
//  ImageGallery
//
//  Created by Lucas Correa on 15/01/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import Foundation

struct Size {
    let label: String
    let width: Int
    let height: Int
    let source: String
    let url: String
    
    enum CodingKeys: String, CodingKey {
        case label
        case width
        case height
        case source
        case url
    }
}

// MARK: - Decodable
extension Size: Decodable {
    
}
