//
//  SizeContainer.swift
//  ImageGallery
//
//  Created by Lucas Correa on 15/01/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import Foundation

struct SizeContainer {
    let sizeList: [Size]
    
    enum CodingKeys: String, CodingKey {
        case sizes
        case sizeList = "size"
    }
}

// MARK: - Decodable
extension SizeContainer: Decodable {
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let sizes = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .sizes)
        
        sizeList = try sizes.decode([Size].self, forKey: .sizeList)
    }
    
}
