//
//  FlickrClient.swift
//  ImageGallery
//
//  Created by Lucas Correa on 14/01/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import Foundation

class FlickrClient: NetworkClient {
    
    private var sessionTask: URLSessionTask?
    
    //
    // MARK: - Public Functions
    
    /// This method is responsible for requesting with the API.
    /// - Parameters:
    ///   - router: Router contains which method, path, headers, parameters and url will be used in the request.
    ///   - returnType: Which is the type of model returned in the result.
    ///   - completion: Result<Type of model, NetworkError>
    func request<T>(router: Router, returnType: T.Type, completion: @escaping (Result<T, NetworkError>) -> Void) where T : Decodable {
        
        guard let url = router.urlComponents.url else {
            return completion(.failure(.invalidURL))
        }
        
        var urlRequest = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: Config.TIMEOUT)
        urlRequest.httpMethod = router.method
        
        for (key, value) in router.headers {
            urlRequest.setValue(value, forHTTPHeaderField: key)
        }
        
        //https://developer.apple.com/forums/thread/123346
        //https://stackoverflow.com/questions/58011737/ios-13-tls-issue
        
        //After some tests when some scroll is performed this error happens below, I put some link, but I still couldn't solve it so I preferred to disable the SSL feature.
        //Connection : unable to determine interface type without an established connection
        //Connection : unable to determine fallback status without a connection
        
//        let session = URLSession(configuration: .ephemeral,
//                                 delegate: SSLPinningDelegate(),
//                                 delegateQueue: nil)
        
        sessionTask = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            guard let data = data, error == nil else {
                if let error = error as NSError?, error.domain == NSURLErrorDomain && error.code != NSURLErrorCancelled {
                    completion(.failure(.domainError))
                }
                return
            }
            
            if let response = response as? HTTPURLResponse {
                // 200 OK, 201 Created, 202 Accepted, 204 No content (PUT, POST or DELETE)
                if (200...204).contains(response.statusCode) {
                    do {
                        let object = try JSONDecoder().decode(returnType, from: data)
                        completion(.success(object))
                    } catch {
                        // print(error)
                        completion(.failure(.decodingError(message: error.localizedDescription)))
                    }
                } else {
                    completion(.failure(.statusCodeError(message: "Unexpected statusCode", code: response.statusCode)))
                }
            }
        }
        
        sessionTask?.resume()
    }
    
    /// Cancel network request
    func cancelNetworkRequest() {
        //https://developer.apple.com/documentation/foundation/urlsessiontask/1411591-cancel
        //https://forums.swift.org/t/silence-alamofire-999-error-handling/28522
        if sessionTask?.state == URLSessionTask.State.running {
            sessionTask?.cancel()
        }
    }
    
}

