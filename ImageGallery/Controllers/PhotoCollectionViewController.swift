//
//  PhotoCollectionViewController.swift
//  ImageGallery
//
//  Created by Lucas Correa on 13/01/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import UIKit

class PhotoCollectionViewController: UIViewController {
    
    @IBOutlet var collectionView: UICollectionView!
    //
    // MARK: - IBOutlets
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //
    // MARK: - Properties
    private let viewModel: PhotoCollectionViewModel
    private var fullScreenViewController: FullScreenViewController?
    var searchTag: String = "" {
        didSet {
            removeAllItems()
            loadPhotos()
        }
    }
    
    /// Init
    /// - Parameters:
    ///   - viewModel: PhotoCollectionViewModel
    init(viewModel: PhotoCollectionViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("You must create this view controller with a ViewModel.")
    }
    
    //
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    //
    // MARK: - Public methods
    
    //
    // MARK: - Private methods
    
    private func setup() {
        title = viewModel.title
        collectionView.prefetchDataSource = self
        collectionView.register(PhotoCollectionViewCell.self)
        collectionView.registerHeader(SearchCollectionReusableView.self)
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.sectionHeadersPinToVisibleBounds = true
            layout.headerReferenceSize = CGSize(width: self.view.frame.width, height: 58)
        }
    }
    
    /// Load photo list
    private func loadPhotos() {
        
        let page = String(viewModel.currentPage)
        
        if viewModel.currentPage == 1 {
            activityIndicator.isHidden = false
        }
        
        viewModel.searchPhotos(by: searchTag, page: page, success: { [weak self] indexPaths in
            guard let self = self else { return }
            
            DispatchQueue.main.async {
                self.viewModel.isFetching = false
                self.activityIndicator.isHidden = true
                
                if indexPaths.count > 0 {
                    self.collectionView.insertItems(at: indexPaths)
                }
            }
        }) { [weak self] error in
            print(error)
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.activityIndicator.isHidden = true
                let alertController = UIAlertController(title: "Alert", message: "Sorry, we were unable to query the tag, please try again later.", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    /// Open full screen image by photo model
    /// - Parameter photo: Model
    private func openFullScreenImage(photo: Photo) {
        navigationController?.setNavigationBarHidden(true, animated: true)
        
        //Dependency Injection
        let viewModel = FullScreenViewModel(photo: photo)
        fullScreenViewController = FullScreenViewController(viewModel: viewModel)
        guard let fullScreenViewController = fullScreenViewController else { return }
        fullScreenViewController.view.frame = view.bounds
        view.addSubview(fullScreenViewController.view)
        
        fullScreenViewController.showImage { [weak self] in
            guard let self = self else { return }
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
    }
    
    /// Clear all items from collectionView and photoItems
    private func removeAllItems() {
        viewModel.currentPage = 1
        viewModel.removeAllPhotos()
        collectionView.reloadData()
    }
    
}

//
// MARK: UICollectionViewDataSource
extension PhotoCollectionViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.photoItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PhotoCollectionViewCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
        
        let photo = viewModel.photoItems[indexPath.row]
        let cellViewModel = PhotoCollectionViewCellViewModel(photo: photo)
        
        cell.configure(withViewModel: cellViewModel, queue: viewModel.queue) { [weak self] in
            guard let self = self else { return }
            DispatchQueue.main.async {
                guard let cell = collectionView.cellForItem(at: indexPath) as? PhotoCollectionViewCell,
                    let operations = self.viewModel.operations[indexPath],
                    let operation = operations.compactMap({ $0 as? ImageDownloadOperation }).first else { return }
                cell.photoImageView.image = operation.image
            }
        }
        
        viewModel.operations[indexPath] = cellViewModel.operations
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if (kind == UICollectionView.elementKindSectionHeader) {
            let headerView:SearchCollectionReusableView = collectionView
                .dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader,
                                                  forIndexPath: indexPath)
            
            headerView.searchBar.delegate = self
            return headerView
        }
        
        return UICollectionReusableView()
    }
    
}

//
// MARK: UICollectionViewDelegate
extension PhotoCollectionViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        viewModel.operationsCancel(indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let photo = viewModel.photoItems[indexPath.row]
        openFullScreenImage(photo: photo)
    }
}

// MARK: - UICollectionViewDataSourcePrefetching
extension PhotoCollectionViewController: UICollectionViewDataSourcePrefetching {
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        
        let needsFetch = indexPaths.contains { $0.row >= viewModel.photoItems.count - 1 } && !viewModel.isFetching
        
        if needsFetch {
            viewModel.isFetching = true
            viewModel.currentPage += 1
            loadPhotos()
        }
    }
}


//
// MARK: - UICollectionViewDelegateFlowLayout
extension PhotoCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.width - 30)/2
        return CGSize(width: width, height: width)
    }
    
}

//
// MARK: - UISearchBarDelegate
extension PhotoCollectionViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
        
        if let searchText = searchBar.text, !searchText.isEmpty {
            searchTag = searchText
        }
    }
}
