//
//  PhotoCollectionViewModelTests.swift
//  ImageGalleryTests
//
//  Created by Lucas Correa on 16/01/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import XCTest

@testable import ImageGallery

class PhotoCollectionViewModelTests: XCTestCase {
    
    var photoCollectionViewModel: PhotoCollectionViewModel!
    var flickrClient: MockFlickrClient!
    
    override func setUp() {
        flickrClient = MockFlickrClient()
        let service = FlickrService(client: flickrClient)
        photoCollectionViewModel = PhotoCollectionViewModel(service: service)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testPhotoCollectionViewModelHasExpectedPhotosListCount() {
        
        photoCollectionViewModel.searchPhotos(by: "kitten", page: "1", success: { [weak self] _ in
            XCTAssertEqual(self?.photoCollectionViewModel.photoItems.count, 5)
        }) { error in
            XCTFail()
        }
    }
    
    func testPhotoCollectionViewModelHasExpectedRemoveAllPhotoItems() {
        photoCollectionViewModel.searchPhotos(by: "kitten", page: "1", success: { [weak self] _ in
            XCTAssertEqual(self?.photoCollectionViewModel.photoItems.count, 5)
            self?.photoCollectionViewModel.removeAllPhotos()
            XCTAssertEqual(self?.photoCollectionViewModel.photoItems.count, 0)
        }) { error in
            XCTFail()
        }
    }
    
    func testPhotoCollectionViewModelHasExpectedCannotFindEndpoint() {
        flickrClient.shouldReturnError = true
        
        photoCollectionViewModel.searchPhotos(by: "kitten", page: "1", success: { _ in
            XCTFail()
        }) { error in
            XCTAssertNotNil(error)
        }
    }
}
