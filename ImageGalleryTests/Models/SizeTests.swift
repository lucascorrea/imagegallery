//
//  SizeTests.swift
//  ImageGalleryTests
//
//  Created by Lucas Correa on 16/01/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import XCTest

@testable import ImageGallery

class SizeTests: ModelCustomTests {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSizeHasExpectedToBeEmpty() {
        let size = Size(label: "", width: 0, height: 0, source: "", url: "")
        XCTAssertEqual(size.label, "")
        XCTAssertEqual(size.width, 0)
        XCTAssertEqual(size.height, 0)
        XCTAssertEqual(size.source, "")
        XCTAssertEqual(size.url, "")
    }
    
    func testSizeHasExpectedTheLabel() {
        let size = makeSUT()
        XCTAssertEqual(size.label, "Large Square")
        XCTAssertNotEqual(size.label, "Original")
    }
    
    func testSizeHasExpectedTheWidth() {
        let size = makeSUT()
        XCTAssertEqual(size.width, 150)
        XCTAssertNotEqual(size.width, 200)
    }
    
    func testSizeHasExpectedTheHeight() {
        let size = makeSUT()
        XCTAssertEqual(size.height, 150)
        XCTAssertNotEqual(size.height, 200)
    }
    
    func testSizeHasExpectedTheSource() {
        let size = makeSUT()
        XCTAssertEqual(size.source, "https://live.staticflickr.com/65535/49392635048_67b42dd973_q.jpg")
        XCTAssertNotEqual(size.source, "https://live.staticflickr.com/65535/49392635048_5feab5a1d9_o.jpg")
    }
    
    func testSizeHasExpectedTheUrl() {
        let size = makeSUT()
        XCTAssertEqual(size.url, "https://www.flickr.com/photos/stolensoul/49392635048/sizes/q/")
        XCTAssertNotEqual(size.url, "https://www.flickr.com/photos/stolensoul/49392635048/sizes/o/")
    }
    
    func testSizeEncoding() throws {
        let size = makeSUT()
        XCTAssertNoThrow(try JSONEncoder().encode(size))
    }
    
    func testSizeDecoding() throws {
        XCTAssertNoThrow(try JSONDecoder().decode(Size.self, from: makeData(resource: "size", withExtension: "json")))
    }
    
    func testSizeDecodingVariables() throws {
        let size = try JSONDecoder().decode(Size.self, from: makeData(resource: "size", withExtension: "json"))
        
        XCTAssertEqual(size.label, "Large Square")
        XCTAssertEqual(size.width, 150)
        XCTAssertEqual(size.height, 150)
        XCTAssertEqual(size.source, "https://live.staticflickr.com/65535/49392635048_67b42dd973_q.jpg")
        XCTAssertEqual(size.url, "https://www.flickr.com/photos/stolensoul/49392635048/sizes/q/")
    }
    
    func makeSUT() -> Size {
        let size = Size(label: "Large Square", width: 150, height: 150, source: "https://live.staticflickr.com/65535/49392635048_67b42dd973_q.jpg", url: "https://www.flickr.com/photos/stolensoul/49392635048/sizes/q/")
        return size
    }
}

extension Size: Encodable {
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(label, forKey: .label)
        try container.encode(width, forKey: .width)
        try container.encode(height, forKey: .height)
        try container.encode(source, forKey: .source)
        try container.encode(url, forKey: .url)
    }
}
