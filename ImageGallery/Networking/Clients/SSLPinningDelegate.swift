//
//  SSLPinningDelegate.swift
//  ImageGallery
//
//  Created by Lucas Correa on 30/08/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import Foundation

class SSLPinningDelegate: NSObject {

    // MARK: - Private methods
    
    private func validate(trust: SecTrust, with policy: [SecPolicy]) -> Bool {
        let status = SecTrustSetPolicies(trust, policy as CFTypeRef)
        guard status == errSecSuccess else { return false }
        
        return SecTrustEvaluateWithError(trust, nil)
    }
    
}

// MARK: - URLSessionDelegate
extension SSLPinningDelegate: URLSessionDelegate {
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        guard let serverTrust = challenge.protectionSpace.serverTrust,
            let certificate = SecTrustGetCertificateAtIndex(serverTrust, 0),
            let pathToCert = Bundle.main.path(forResource: "flickr.com", ofType: "cer"),
            let localCertificate = NSData(contentsOfFile: pathToCert) else {
                completionHandler(.cancelAuthenticationChallenge, nil)
                return
        }
        
        // Set SSL policies for domain name check
        var policies = [SecPolicy]()
        policies.append(SecPolicyCreateSSL(true, (challenge.protectionSpace.host as CFString)))
        policies.append(SecPolicyCreateBasicX509())
        
        let isServerTrusted = validate(trust: serverTrust, with: policies)
        
        // Get local and remote cert data
        let remoteCertificateData:NSData = SecCertificateCopyData(certificate)
        
        if (isServerTrusted && remoteCertificateData.isEqual(to: localCertificate as Data)) {
            let credential:URLCredential = URLCredential(trust: serverTrust)
            completionHandler(.useCredential, credential)
        } else {
            completionHandler(.cancelAuthenticationChallenge, nil)
        }
    }
}
