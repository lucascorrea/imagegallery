//
//  UICollectionViewCell+Ext.swift
//  ImageGallery
//
//  Created by Lucas Correa on 29/08/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import UIKit

extension UICollectionViewCell {
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
}
