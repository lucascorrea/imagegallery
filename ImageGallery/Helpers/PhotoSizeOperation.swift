//
//  PhotoSizeOperation.swift
//  ImageGallery
//
//  Created by Lucas Correa on 29/08/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import Foundation

protocol PhotoSizeDataProvider {
    var imageUrl: URL? { get }
}

enum SizeLabel: String {
    case largeSquare = "Large Square"
    case large = "Large"
}

final class PhotoSizeOperation: AsyncOperation {
    
    //
    // MARK: - Properties
    private var size: Size?
    private let service: FlickrService
    private let photoId: String
    private let sizeLabel: SizeLabel
    
    //
    // MARK: - Initializer DI
    init(photoId: String, sizeLabel: SizeLabel, service: FlickrService = FlickrService(client: FlickrClient())) {
        self.photoId = photoId
        self.service = service
        self.sizeLabel = sizeLabel
        super.init()
    }
    
    //
    // MARK: - Methods
    override func main() {
        guard isCancelled == false else { return }
        
        service.getPhoto(photoId: photoId) { [weak self] result in
            guard let self = self else { return }
            
            defer { self.state = .finished }
            
            switch result {
                case .success(let sizeContainer):
                    if let sizelabel = (sizeContainer.sizeList
                        .filter { $0.label == self.sizeLabel.rawValue }
                        .first) {
                        self.size = sizelabel
                }
                case .failure(let error):
                    print(error)
            }
        }
    }
    
    override func cancel() {
        super.cancel()
        service.cancelProcess()
    }
}

//
// MARK: - PhotoSizeDataProvider
extension PhotoSizeOperation: PhotoSizeDataProvider {
    var imageUrl: URL? {
        guard let urlString = size?.source,
            let url = URL(string: urlString) else { return nil }
        return url
    }
}
