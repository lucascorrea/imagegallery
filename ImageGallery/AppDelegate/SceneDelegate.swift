//
//  SceneDelegate.swift
//  ImageGallery
//
//  Created by Lucas Correa on 13/01/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
          
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            
            //Dependency Injection
            let viewModel = PhotoCollectionViewModel()
            let photoCollectionViewController = PhotoCollectionViewController(viewModel: viewModel)
            
            let navigationController = UINavigationController(rootViewController: photoCollectionViewController)
            navigationController.navigationBar.isTranslucent = false
            window.rootViewController = navigationController
            
            self.window = window
            window.makeKeyAndVisible()
        }
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {}
    func sceneDidBecomeActive(_ scene: UIScene) {}
    func sceneWillResignActive(_ scene: UIScene) {}
    func sceneWillEnterForeground(_ scene: UIScene) {}
    func sceneDidEnterBackground(_ scene: UIScene) {}
}

