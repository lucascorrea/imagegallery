//
//  PhotoCollectionViewCell.swift
//  ImageGallery
//
//  Created by Lucas Correa on 15/01/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    //
    // MARK: - IBOultets
    @IBOutlet weak var photoImageView: UIImageView!
    
    //
    // MARK: - Properties
    private var downloadCompletion: SuccessHandler?
    
    // MARK: - Public Functions
    func configure(withViewModel viewModel:PhotoCollectionViewCellViewModel,
                   queue: OperationQueue,
                   success completion: @escaping SuccessHandler) {
        downloadCompletion = completion
        photoImageView.image = #imageLiteral(resourceName: "EmptyIcon")
        
        viewModel.downloadImage(queue: queue) { [weak self] in
            guard let self = self else { return }
            self.downloadCompletion?()
        }
    }
}
