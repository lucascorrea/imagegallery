//
//  URLComponents+Ext.swift
//  ImageGallery
//
//  Created by Lucas Correa on 25/08/2020.
//  Copyright © 2020 SiriusCode. All rights reserved.
//

import Foundation

extension URLComponents {
    
    mutating func setQueryItems(with parameters: [String: String]) {
        self.queryItems = parameters.map { URLQueryItem(name: $0.key, value: $0.value) }
    }
    
}
